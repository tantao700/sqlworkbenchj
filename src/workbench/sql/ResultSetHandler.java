package workbench.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author T_T on 2018/9/8.
 */
public abstract class ResultSetHandler<T> {

    public abstract T handle(ResultSet rs) throws SQLException;

}