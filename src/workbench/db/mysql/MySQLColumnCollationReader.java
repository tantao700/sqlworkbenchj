/*
 * MySQLColumnCollationReader.java
 *
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
package workbench.db.mysql;

import lombok.extern.slf4j.Slf4j;
import workbench.db.ColumnIdentifier;
import workbench.db.TableDefinition;
import workbench.db.WbConnection;
import workbench.sql.ResultSetHandler;
import workbench.util.StringUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Thomas Kellerer
 */
@Slf4j
public class MySQLColumnCollationReader {

    public void readCollations(TableDefinition table, WbConnection conn) throws SQLException {

        Info info = conn.execQuery(
                "show variables where variable_name in ('collation_database', 'character_set_database')",
                new ResultSetHandler<Info>() {
                    @Override
                    public Info handle(ResultSet rs) throws SQLException {
                        Info info2 = new Info();
                        while (rs.next()) {
                            String name = rs.getString(1);
                            String value = rs.getString(2);
                            if ("character_set_database".equals(name)) {
                                info2.defaultCharacterSet = value;
                            }
                            if ("collation_database".equals(name)) {
                                info2.defaultCollation = value;
                            }
                        }
                        return info2;
                    }
                }
        );

        // In MySQL 5.7 show variables is no longer available to regular users
        // in that case both defaults will be null --> don't check anything
        if (info.defaultCharacterSet == null && info.defaultCollation == null) return;

        Map<String, String> collations = new HashMap<>(table.getColumnCount());
        Map<String, String> expressions = new HashMap<>(table.getColumnCount());
        conn.execQuery(
                " SELECT  column_name,character_set_name,collation_name FROM information_schema.columns WHERE table_name = ? AND table_schema = ? ",
                new ResultSetHandler<Info>() {
                    @Override
                    public Info handle(ResultSet rs) throws SQLException {
                        while (rs.next()) {
                            String columnName = rs.getString(1);
                            String charset = rs.getString(2);
                            String collation = rs.getString(3);
                            String expression = null;
                            if (isNonDefault(collation, info.defaultCollation) && isNonDefault(charset, info.defaultCharacterSet)) {
                                expression = "CHARSET " + charset + " COLLATE " + collation;
                            } else if (isNonDefault(collation, info.defaultCollation)) {
                                expression = "COLLATE " + collation;
                            } else if (isNonDefault(charset, info.defaultCharacterSet)) {
                                expression = "CHARSET " + charset;
                            }

                            if (expression != null) {
                                expressions.put(columnName, expression);
                            }
                            if (collation != null) {
                                collations.put(columnName, collation);
                            }
                        }
                        return null;
                    }
                },
                new HashMap<Integer,Object>(){{
                    put(1,table.getTable().getTableName());
                    put(2,table.getTable().getCatalog());
                }}
        );

        for (ColumnIdentifier col : table.getColumns()) {
            String expression = expressions.get(col.getColumnName());
            if (expression != null) {
                String dataType = col.getDbmsType() + " " + expression;
                col.setDbmsType(dataType);
            }
            String collation = collations.get(col.getColumnName());
            if (collation != null) {
                col.setCollation(collation);
            }
        }
    }

    private boolean isNonDefault(String value, String defaultValue) {
        return defaultValue != null
                && !StringUtil.isEmpty(value)
                && !value.equals(defaultValue);
    }

    private class Info {
        String defaultCharacterSet;
        String defaultCollation;
    }


}
