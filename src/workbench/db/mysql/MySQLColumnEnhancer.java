/*
 * MySQLColumnEnhancer.java
 *
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
package workbench.db.mysql;

import lombok.extern.slf4j.Slf4j;
import workbench.db.*;
import workbench.resource.Settings;
import workbench.sql.ResultSetHandler;
import workbench.util.SqlUtil;
import workbench.util.StringUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * A class to retrieve enum and collation definitions for the columns of a MySQL table.
 *
 * @author Thomas Kellerer
 * @see workbench.db.DbMetadata#getTableDefinition(workbench.db.TableIdentifier)
 * @see MySQLEnumReader
 * @see MySQLColumnCollationReader
 */
@Slf4j
public class MySQLColumnEnhancer
        implements ColumnDefinitionEnhancer {

    @Override
    public void updateColumnDefinition(TableDefinition tbl, WbConnection connection) throws SQLException {
        MySQLColumnCollationReader collationReader = new MySQLColumnCollationReader();
        collationReader.readCollations(tbl, connection);

        MySQLEnumReader enumReader = new MySQLEnumReader();
        enumReader.readEnums(tbl, connection);

        updateComputedColumns(tbl, connection);
    }

    private void updateComputedColumns(TableDefinition tbl, WbConnection connection) throws SQLException {

        String sql = String.format(
                "select column_name, extra, %s from information_schema.columns where table_schema = ?  and table_name = ? ",
                (JdbcUtils.hasMinimumServerVersion(connection, "5.7") && !connection.getMetadata().isMariaDB()) ? "generation_expression" : "null as generation_expression"
        );

        connection.execQuery(
                sql,
                new ResultSetHandler<Object>() {
                    @Override
                    public Object handle(ResultSet rs) throws SQLException {
                        List<ColumnIdentifier> columns = tbl.getColumns();
                        while (rs.next()) {
                            String columnName = rs.getString(1);
                            String extra = rs.getString(2);
                            String expression = rs.getString(3);
                            ColumnIdentifier col = ColumnIdentifier.findColumnInList(columns, columnName);
                            if (col != null) {
                                if (StringUtil.isNonBlank(expression)) {
                                    String genSql = " GENERATED ALWAYS AS (" + expression + ") " + extra.replace("GENERATED", "").trim();
                                    col.setComputedColumnExpression(genSql);
                                } else if (extra != null && extra.toLowerCase().startsWith("on update")) {
                                    String defaultValue = col.getDefaultValue();
                                    if (defaultValue == null) {
                                        col.setDefaultValue(extra);
                                    } else {
                                        defaultValue += " " + extra;
                                        col.setDefaultValue(defaultValue);
                                    }
                                }
                            }
                        }
                        return null;
                    }
                },
                new HashMap<Integer, Object>() {{
                    put(1, tbl.getTable().getRawCatalog());
                    put(2, tbl.getTable().getRawTableName());
                }}
        );
    }
}
