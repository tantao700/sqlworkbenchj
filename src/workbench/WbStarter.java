/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
package workbench;

import lombok.extern.slf4j.Slf4j;
import workbench.resource.ResourceMgr;
import workbench.util.ext.JavaVersion;

import javax.swing.*;
import java.lang.reflect.Method;

/**
 * This is a wrapper to kick-off the actual WbManager class.
 * <p/>
 * It should run with any JDK >= 1.3 as it does not reference any other classes.
 * <br/>
 * This class is compiled separately in build.xml to allow for a different
 * class file version between this class and the rest of the application.
 * Thus a check for the correct JDK version can be done inside the Java code.
 *
 * @author Thomas Kellerer
 */
@Slf4j
public class WbStarter {

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        JavaVersion v = JavaVersion.of(System.getProperty("java.version", System.getProperty("java.runtime.version")));

        int MIN_VERSION = 8;

        if (v.getVersion() - MIN_VERSION < 0) {
            String error =
                    "%s requires Java %d, but only %d was found\n\n" +
                            "If you do have Java 8 installed, please point JAVA_HOME to the location of your Java 8 installation.\n" +
                            "or refer to the manual for details on how to specify the Java runtime to be used.";

            String message = String.format(error, ResourceMgr.TXT_PRODUCT_NAME, MIN_VERSION, MIN_VERSION);

            log.error(message);
            JOptionPane.showConfirmDialog(null, message, "Alert", JOptionPane.DEFAULT_OPTION);
            System.exit(-1);
            return;
        }

        try {
            // Do not reference WbManager directly, otherwise a compile
            // of this class will trigger a compile of the other classes, but they
            // should be compiled with a different class file version (see build.xml)
            Class mgr = Class.forName("workbench.WbManager");
            Method main = mgr.getDeclaredMethod("main", String[].class);
            main.invoke(null, new Object[]{args});
        } catch (Throwable e) {
            log.error("", e);
        }
    }

}
