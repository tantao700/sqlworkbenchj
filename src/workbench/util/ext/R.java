package workbench.util.ext;

import javax.swing.*;

/**
 * @author T_T on 2018/9/7.
 */
public class R {

    public static ImageIcon imageIcon(String resource) {
        return new ImageIcon(R.class.getClassLoader().getResource(resource));
    }
}
