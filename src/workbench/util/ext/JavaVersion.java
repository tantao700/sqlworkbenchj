package workbench.util.ext;

import lombok.Data;

/**
 * @author T_T on 2018/9/8.
 */
@Data
public class JavaVersion {

    private int gen;
    private int version;
    private String update;


    public static JavaVersion of(String versionString) {
        if (!versionString.matches("1\\.[0-9]{1,}(\\.)?(.*)")) {
            throw new RuntimeException("incorrect version:" + versionString);
        }

        String[] strings = versionString.split("\\.");
        JavaVersion version = new JavaVersion();
        version.gen = Integer.valueOf(strings[0]);
        version.version = Integer.valueOf(strings[1]);

        if (strings.length > 2) {
            version.update = strings[2];
        }

        return version;

    }


    public boolean greatEqual(int version) {
        return version >= this.version;
    }

}
