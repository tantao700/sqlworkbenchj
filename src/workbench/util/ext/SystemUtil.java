package workbench.util.ext;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import workbench.interfaces.PrintInfo;
import workbench.util.MemoryWatcher;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author T_T on 2018/9/7.
 */
@Slf4j
public class SystemUtil {

    private static boolean isMacOS = System.getProperty("os.name").startsWith("Mac");

    public static Map<String, String> env(String... properties) {
        Map<String, String> environments = new TreeMap<>();
        for (String variable : properties) {
            environments.put(variable, System.getProperty(variable));
        }
        return environments;
    }

    public static Map<String, String> infoOS() {
        return env("os.name", "os.version", "os.arch");
    }

    public static Map<String, String> infoJava() {
        return env("java.version", "java.home", "java.vendor", "java.vm.name");
    }

    public static Map<String, String> infoMemory() {
        return new HashMap<String, String>() {{
            put("system.memory", MemoryWatcher.MAX_MEMORY / (1024 * 1024) + " MB");
        }};
    }

    public static boolean isMacOS() {
        return isMacOS;
    }

    public static void print(Logger _log, String name, PrintInfo... printInfos) {
        Map<Object, Object> _map = new TreeMap<>();
        for (PrintInfo info : printInfos) {
            _map.putAll(info.getInfo());
        }

        print(_log, name, _map);
    }

    public static void print(Logger _log, String name, Map<?, ?>... maps) {
        Map<Object, Object> _map = new TreeMap<>();

        for (Map<?, ?> map : maps) {
            if (map != null) {
                _map.putAll(map);
            }
        }

        if (_log != null && !_map.isEmpty()) {
            StringBuilder buffer = new StringBuilder();
            buffer.append("\n").append(String.format(">>>>>>>>>>>>>>[%s]>>>>>>>>>>>>>>>>>>", name));
            for (Map.Entry<?, ?> entry : _map.entrySet()) {
                buffer.append("\n").append(String.format("%s:%s", entry.getKey(), entry.getValue()));
            }
            buffer.append("\n").append(String.format("<<<<<<<<<<<<<<[%s]<<<<<<<<<<<<<<<<<<<", name));
            _log.info(buffer.toString());
        }
    }
}
