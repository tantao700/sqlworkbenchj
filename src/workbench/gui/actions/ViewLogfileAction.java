/*
 * ViewLogfileAction.java
 *
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
package workbench.gui.actions;

import lombok.extern.slf4j.Slf4j;
import workbench.WbManager;
import workbench.gui.WbSwingUtilities;
import workbench.gui.components.LogFileViewer;
import workbench.resource.ResourceMgr;
import workbench.util.ExceptionUtil;
import workbench.util.ToolDefinition;
import workbench.util.WbFile;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

/**
 * @author Thomas Kellerer
 */
@Slf4j
public class ViewLogfileAction
        extends WbAction
        implements WindowListener {
    private static ViewLogfileAction instance = new ViewLogfileAction();
    private LogFileViewer viewer = null;

    public static ViewLogfileAction getInstance() {
        return instance;
    }

    private ViewLogfileAction() {
        super();
        this.initMenuDefinition("MnuTxtViewLogfile");
        String tip = ResourceMgr.getFormattedString("d_MnuTxtViewLogfile", Integer.toString(LogFileViewer.getMaxLines()));
        setTooltip(tip);
        this.removeIcon();
//    WbFile logFile = LogMgr.getLogfile();
//    this.setEnabled(logFile != null);
    }

    @Override
    public void executeAction(ActionEvent e) {

    }

    private boolean openWithProgram(String program, WbFile logfile) {
        WbFile tool = new WbFile(program);
        if (!tool.exists()) return false;

        ToolDefinition def = new ToolDefinition(program, null, null);
        try {
            def.runApplication('"' + logfile.getAbsolutePath() + '"');
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    private boolean openWithSystem(final WbFile logfile) {
        try {
            Desktop.getDesktop().open(logfile);
            return true;
        } catch (Exception ex) {
            log.error("ViewLogFileAction.openWithSystem()", "Error when opening logfile", ex);
            WbSwingUtilities.showErrorMessage(ExceptionUtil.getDisplay(ex));
        }
        return false;
    }

    private void openInteralViewer(final WbFile logfile) {
        EventQueue.invokeLater(() ->
        {
            if (viewer == null) {
                try {
                    viewer = new LogFileViewer(WbManager.getInstance().getCurrentWindow());
                    viewer.addWindowListener(ViewLogfileAction.this);
                    viewer.setText(ResourceMgr.getString("LblLoadingProgress"));
                    viewer.setVisible(true);
                    viewer.showFile(logfile);
                } catch (Exception e) {
                    log.error("ViewLogFileAction.executeAction()", "Error displaying the log file", e);
                    WbSwingUtilities.showErrorMessage(ExceptionUtil.getDisplay(e));
                }
            } else {
                viewer.toFront();
            }
        });
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
        viewer = null;
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

}
