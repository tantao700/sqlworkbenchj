/*
 * FileSaveProfiles.java
 *
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
package workbench.gui.actions;

import lombok.extern.slf4j.Slf4j;
import workbench.WbManager;
import workbench.db.ConnectionMgr;
import workbench.gui.WbSwingUtilities;
import workbench.resource.ResourceMgr;
import workbench.util.ExceptionUtil;

import java.awt.event.ActionEvent;

/**
 * Saves the connection profiles
 *
 * @author Thomas Kellerer
 */
@Slf4j
public class FileSaveProfiles
  extends WbAction
{
  public FileSaveProfiles()
  {
    super();
    this.initMenuDefinition("MnuTxtFilesSaveProfiles");
  }

  @Override
  public void executeAction(ActionEvent e)
  {
    try
    {
      ConnectionMgr.getInstance().saveProfiles();
      WbSwingUtilities.showMessage(WbManager.getInstance().getCurrentWindow(), ResourceMgr.getString("MsgProfilesSaved"));
    }
    catch (Exception ex)
    {
      log.error("FileSaveProfiles.executeAction()", "Error saving profiles", ex);
      WbSwingUtilities.showMessage(WbManager.getInstance().getCurrentWindow(), ResourceMgr.getString("ErrSavingProfiles") + "\n" + ExceptionUtil.getDisplay(ex));
    }
  }
}
