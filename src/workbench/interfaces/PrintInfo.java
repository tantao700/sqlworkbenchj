package workbench.interfaces;

import java.util.Map;

/**
 * @author T_T on 2018/9/8.
 */
public interface PrintInfo {

    Map<String, String> getInfo();
}
