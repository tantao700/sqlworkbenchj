package workbench.util.ext;


import org.junit.Assert;
import org.junit.Test;

/**
 * @author T_T on 2018/9/8.
 */
public class JavaVersionTest {

    @Test
    public void testGreatEqual() {
        JavaVersion version = JavaVersion.of("1.8.0_144");
        Assert.assertEquals(8, version.getVersion());
        Assert.assertEquals(1, version.getGen());
        Assert.assertEquals("0_144", version.getUpdate());
    }

    @Test
    public void testVersion10GreatEqual() {
        JavaVersion version = JavaVersion.of("1.11.0_144");
        Assert.assertEquals(11, version.getVersion());
        Assert.assertEquals(1, version.getGen());
        Assert.assertEquals("0_144", version.getUpdate());
    }

    @Test(expected = RuntimeException.class)
    public void testErrorVersion() {
        JavaVersion.of("1.xx.xxxxx");
    }

}