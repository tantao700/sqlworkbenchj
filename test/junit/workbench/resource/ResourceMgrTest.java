/*
 * ResourceMgrTest.java
 *
 * This file is part of SQL Workbench/J, http://www.sql-workbench.net
 *
 * Copyright 2002-2018, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     http://sql-workbench.net/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.net
 *
 */
package workbench.resource;

import org.junit.Test;
import workbench.WbTestCase;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.fail;

/**
 *
 * @author Thomas Kellerer
 */
public class ResourceMgrTest
	extends WbTestCase
{

	public ResourceMgrTest()
	{
		super("ResourceMgrTest");
	}


	private String getString(ResourceBundle bundle, String key)
	{
		try
		{
			return bundle.getString(key);
		}
		catch (Exception ex)
		{
			return null;
		}
	}

	@Test
	public void testQuoting()
	{
		Locale en = new Locale("en");
		Locale de = new Locale("de");

		ResourceBundle enBundle = ResourceMgr.getResourceBundle(en);
		checkQuoting(enBundle);
		ResourceBundle deBundle = ResourceMgr.getResourceBundle(de);
		checkQuoting(deBundle);
	}

	private void checkQuoting(ResourceBundle bundle)
	{
		Pattern p = Pattern.compile("\\s+'\\{[0-9]+\\}'\\s+");
		for (String key : bundle.keySet())
		{
			String value = bundle.getString(key);
			Matcher m = p.matcher(value);
			if (m.find())
			{
				fail("Key=" + key + " for language " + bundle.getLocale() + " uses incorrect single quotes for parameter marker");
			}
		}
	}

}
